ThisBuild / name := "story-sudanese"
ThisBuild / version := "0.1.0"
ThisBuild / scalaVersion := "2.12.8"

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint:_",
  "-Xverify",
)

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.5.13"
)

lazy val engine = (project in file("."))

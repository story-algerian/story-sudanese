/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.i18n

/* This has stuff like `null` and java imports because this is a port
 * of Java code from StackOverflow lol
 */

import java.io.{InputStream, InputStreamReader}
import java.util.{Locale, PropertyResourceBundle, ResourceBundle}

final class UTF8Control extends ResourceBundle.Control {
  override def newBundle(baseName: String, locale: Locale, format: String, loader: ClassLoader, reload: Boolean): ResourceBundle = {
    val bundleName = toBundleName(baseName, locale)
    val resName = toResourceName(bundleName, "properties")
    val stream: InputStream = if (reload) {
      val url = loader.getResource(resName)
      if (url != null) {
        val conn = url.openConnection
        if (conn != null) {
          conn.setUseCaches(false)
          conn.getInputStream
        } else {
          null
        }
      } else {
        null
      }
    } else {
      loader.getResourceAsStream(resName)
    }

    if (stream != null) {
      try {
        new PropertyResourceBundle(new InputStreamReader(stream, "UTF-8"))
      } finally {
        stream.close()
      }
    } else {
      null
    }
  }
}

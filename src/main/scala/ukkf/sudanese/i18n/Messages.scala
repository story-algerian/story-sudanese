/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.i18n

import java.util.ResourceBundle

object Messages {
  def getMsgs(bundleName: String, packageName: String = "ukkf.sudanese"): ResourceBundle =
    ResourceBundle.getBundle(s"${packageName}.${bundleName}", new UTF8Control())

  def fmt(format: String, objs: Any*): String = format.format(objs : _*)
}

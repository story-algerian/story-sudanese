/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.i18n

object ListBuilder {
  val msgs = Messages.getMsgs("misc")

  def buildList(items: String*): String = _buildList(items)

  def buildOrList(items: String*): String =
    Messages.fmt(msgs.getString("or_list"), _buildList(items))

  private def _buildList(items: Seq[String]): String = items.length match {
    case 0 => ""
    case _ =>
      val sb = new StringBuilder
      sb ++= items.head
      items.tail foreach {
        case item =>
          sb ++= ", "
          sb ++= item
      }
      sb.toString
  }
}

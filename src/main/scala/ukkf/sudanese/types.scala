/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.types

import enumeratum._

sealed trait Type extends EnumEntry {
  override def toString(): String = this match {
    case Type.None     => "None"
    case Type.Integer  => "Int"
    case Type.Rational => "Rational"
    case Type.String   => "String"
    case Type.Bool     => "Bool"
  }
}

object Type extends Enum[Type] {
  val values = findValues

  case object None     extends Type
  case object Integer  extends Type
  case object Rational extends Type
  case object String   extends Type
  case object Bool     extends Type
}

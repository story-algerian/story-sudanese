/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.vars

import scala.collection.mutable.HashMap
import ukkf.sudanese.ast.expr.Expr
import ukkf.sudanese.io.err.{MultipleVarDef, UnknownIdentifier}
import ukkf.sudanese.io.Logger

@SerialVersionUID(201L)
final class Variables extends Serializable {
  private[vars] val vars = HashMap[String, Variable]()

  def defineVar(name: String, line: Int, log: Logger): Option[Unit] =
    vars.get(name) match {
      case None =>
        vars += (name -> Variable(line))
        Some(())

      case Some(_var) =>
        log.err(line, None, MultipleVarDef(name, _var.defLine))
        None
    }

  def getVar(name: String, line: Int, column: Int, log: Logger): Option[Variable] =
    vars.get(name) match {
      case None =>
        log.err(line, Some(column), UnknownIdentifier(name))
        None

      case Some(v) => Some(v)
    }

  def setVar(name: String, expr: Expr, line: Int, log: Logger): Option[Unit] =
    vars.get(name) match {
      case None =>
        log.err(line, None, UnknownIdentifier(name))
        None

      case Some(v) => expr.getType(log) match {
        case None => None
        case Some(t) => expr.getValue(log) match {
          case None => None
          case Some(value) =>
            vars += (name -> Variable(v.defLine, t, value))
            Some(())
        }
      }
    }
}

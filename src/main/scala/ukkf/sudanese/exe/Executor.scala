/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.exe

import ukkf.sudanese.ast.block.CodeBlock
import ukkf.sudanese.io.{Color, Logger}
import ukkf.sudanese.vars.Variables

trait Executor {
  def speed: Long
  def speed_=(sp: BigInt): Unit
  def vars: Variables

  def addChoice(display: String, target: CodeBlock, log: Logger): Option[Unit]
  def badEnd(log: Logger): Option[Unit]
  def choose(log: Logger): Option[Unit]
  def chooseRandom(log: Logger): Option[Unit]
  def clearScreen(log: Logger): Option[Unit]
  def goodEnd(log: Logger): Option[Unit]
  def output(text: String, log: Logger): Option[Unit]
  def sleep(time: BigInt, log: Logger): Option[Unit]
  def useColor(col: Color, log: Logger): Option[Unit]
  def waitLine(log: Logger): Option[Unit]
}

/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.block

import scala.collection.mutable.ArrayBuffer
import ukkf.sudanese.ast.expr.{BoolLitExpr, Expr}
import ukkf.sudanese.ast.stmt._
import ukkf.sudanese.io.err.{ElseAfterElse, LabelInsideConditional, NotInIf}
import ukkf.sudanese.io.warn.{EmptyElse, EmptyElseIf, EmptyIf, UnreachableCode}
import ukkf.sudanese.io.Logger
import ukkf.sudanese.tok.TokenReader
import ukkf.sudanese.vars.Variables

final object BlockBuilder {
  def buildBlock(vars: Variables, in: TokenReader, log: Logger): Option[CodeBlock] = {
    val buf = new ArrayBuffer[Statement]
    var lastStatement: Statement = StatementBuilder.buildStmt(vars, in, log) match {
      case None => return None
      case Some(s) => s
    }

    buf += lastStatement
    while (!lastStatement.terminating) {
      if (lastStatement.branchEnd) {
        log.err(lastStatement.line, None, NotInIf)
        return None
      }

      lastStatement = StatementBuilder.buildStmt(vars, in, log) match {
        case None => return None
        case Some(s) => s
      }

      buf += lastStatement
    }

    Some(new CodeBlock(buf.head.line, buf.toArray, true))
  }

  def buildIfBlock(vars: Variables, in: TokenReader, expr: Expr, _line: Int,
                   log: Logger): Option[IfElseBlock] = {
    val branches = new ArrayBuffer[(Expr, CodeBlock)]
    var currentBranch = (expr, new ArrayBuffer[Statement], _line)
    var hasElse = false
    var terminatedCurrent = false
    var end = false

    while (!end) {
      var lastStatement: Statement = StatementBuilder.buildStmt(vars, in, log) match {
        case None => return None
        case Some(s) => s
      }

      while (!lastStatement.branchEnd) {
        if (terminatedCurrent) {
          log.warn(lastStatement.line, None, UnreachableCode)
        } else {
          currentBranch._2 += lastStatement
        }

        if (lastStatement.terminating) {
          terminatedCurrent = true
          lastStatement match {
            case ImplicitGoToStatement(l, _) =>
              log.err(l, None, LabelInsideConditional)
              return None

            case _ => {}
          }
        }

        lastStatement = StatementBuilder.buildStmt(vars, in, log) match {
          case None => return None
          case Some(s) => s
        }
      }

      val line = currentBranch._3

      if (currentBranch._2.length == 0) {
        if (hasElse) {
          log.warn(line, None, EmptyElse)
        } else if (branches.length == 0) {
          log.warn(line, None, EmptyIf)
        } else {
          log.warn(line, None, EmptyElseIf)
        }
      }

      branches += new Tuple2(
        currentBranch._1,
        new CodeBlock(line, currentBranch._2.toArray, terminatedCurrent)
      )

      terminatedCurrent = false

      lastStatement match {
        case ElseStatement(l) =>
          if (hasElse) {
            log.err(l, None, ElseAfterElse(line))
            return None
          } else {
            hasElse = true
            currentBranch = (BoolLitExpr(-1, -1, true),
                             new ArrayBuffer[Statement], l)
          }

        case ElseIfStatement(l, expr) =>
          if (hasElse) {
            log.err(l, None, ElseAfterElse(currentBranch._2.head.line))
            return None
          } else {
            currentBranch = (expr, new ArrayBuffer[Statement], l)
          }

        case EndStatement(l) => end = true

        case _ => return None // This should never happen TODO
      }
    }

    val elseB = if (hasElse) {
      Some(branches.remove(branches.length - 1)._2)
    } else {
      None
    }

    Some(new IfElseBlock(_line, branches.toArray, elseB))
  }
}

/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.block

import ukkf.sudanese.io.Logger
import ukkf.sudanese.io.err.LabelNotFound

final case class BlockResolver(lookup: Seq[(Int, String, CodeBlock)]) {
  def apply(name: String, line: Int, log: Logger): Option[CodeBlock] = {
    // First check if there is any block following the statement
    // with a matching label
    lookup.dropWhile { _._1 < line }.find { _._2 == name } match {
      case Some((_, _, b)) => Some(b)
      case None =>
        // If none was found following the statement, start search
        // from the beginning of the file
        lookup.find { _._2 == name } match {
          case Some((_, _, b)) => Some(b)
          case None =>
            log.err(line, None, LabelNotFound(name))
            None
        }
    }
  }
}

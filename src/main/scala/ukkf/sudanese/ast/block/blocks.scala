/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.block

import ukkf.sudanese.ast.expr.Expr
import ukkf.sudanese.ast.stmt._
import ukkf.sudanese.exe.Executor
import ukkf.sudanese.io.err.{ChoiceQueueEmpty, TypeMismatch}
import ukkf.sudanese.io.Logger
import ukkf.sudanese.types.Type

/** Provides a common interface to blocks in Story African */
@SerialVersionUID(201L)
sealed abstract class Block(val line: Int) extends Serializable {
  def checkChoiceQueue(log: Logger): Option[Unit]
  def execute(exe: Executor, log: Logger): Option[Unit]
  def isTerminated: Boolean
  def minimumChoiceQueueSize: Int
  def resolve(resolver: BlockResolver, log: Logger): Option[Unit]
  def toString: String
}

class CodeBlock(
    l: Int, private var stmts: Array[Statement],
    override val isTerminated: Boolean
  ) extends Block(l) {

  private var minChoiceQ = 0
  private var parents = Array[Block]()

  def addParent(parent: Block): Unit = {
    parents = parents :+ parent
  }

  override def checkChoiceQueue(log: Logger): Option[Unit] = {
    stmts.foreach { _ match {
      case IfStatement(_, b) =>
        b.checkChoiceQueue(log) match {
          case None => return None
          case Some(_) => {}
        }

      case ChooseStatement(l) =>
        if (minimumChoiceQueueSize == 0) {
          log.err(l, None, ChoiceQueueEmpty)
          return None
        }
      case _ => {}
    }}

    Some(())
  }

  override def execute(exe: Executor, log: Logger): Option[Unit] = {
    for (stmt <- stmts) {
      stmt.execute(exe, log) match {
        case None => return None
        case Some(_) => {}
      }
    }
    Some(())
  }

  override def minimumChoiceQueueSize =
    if (parents.isEmpty)
      minChoiceQ
    else
      minChoiceQ + parents.map { _.minimumChoiceQueueSize }.min

  override def resolve(resolver: BlockResolver, log: Logger): Option[Unit] = {
    stmts = stmts.map {
      _ match {
        case AddChoiceStatement(l, str, target) =>
          minChoiceQ += 1
          val resolved = resolver(target, l, log) match {
            case None => return None
            case Some(b) => b
          }

          resolved.addParent(this)
          AddChoiceStatementFinal(l, str, resolved)

        case GoToStatement(l, target) =>
          val resolved = resolver(target, l, log) match {
            case None => return None
            case Some(b) => b
          }

          resolved.addParent(this)
          GoToStatementFinal(l, resolved)

        case s @ IfStatement(_, b) =>
          b.resolve(resolver, log) match {
            case None => return None
            case Some(_) => {}
          }

          minChoiceQ += b.minimumChoiceQueueSize
          s

        case ImplicitGoToStatement(l, target) =>
          val resolved = resolver(target, l, log) match {
            case None => return None
            case Some(b) => b
          }

          resolved.addParent(this)
          GoToStatementFinal(l, resolved)

        case s => s
      }
    }

    Some(())
  }

  override def toString = {
    val sb = new StringBuilder
    stmts foreach {
      case stmt =>
        sb ++= stmt.toString
    }

    sb.toString
  }
}

class IfElseBlock(
  l: Int,
  private var branches: Array[(Expr, CodeBlock)],
  private var elseB: Option[CodeBlock]) extends Block(l) {

  override def checkChoiceQueue(log: Logger): Option[Unit] = {
    branches.foreach {
      case (_, b) => b.checkChoiceQueue(log) match {
        case None => return None
        case Some(_) => {}
      }
    }
    elseB match {
      case None => Some(())
      case Some(b) => b.checkChoiceQueue(log)
    }
  }

  override def execute(exe: Executor, log: Logger): Option[Unit] = {
    for ((expr, branch) <- branches) {
      expr.getType(log).map {
        case Type.Bool => expr.getValue(log) match {
          case None => return None
          case Some(b) if b == true =>
            return branch.execute(exe, log)
          case Some(_) => {}
        }

        case t =>
          log.err(l, None, TypeMismatch(t, Type.Bool))
          return None
      } match {
        case None => return None
        case Some(_) => {}
      }
    }

    elseB match {
      case Some(b) => b.execute(exe, log)
      case None => Some(())
    }
  }

  override def isTerminated: Boolean = {
    elseB match {
      case None => return false
      case Some(b) if (!b.isTerminated) => return false
      case Some(b) => branches foreach {
        case (_, b) if (!b.isTerminated) => return false
        case _ => {}
      }
    }

    return true
  }

  override def minimumChoiceQueueSize = elseB match {
    case Some(br) =>
      val a = br.minimumChoiceQueueSize
      val b = branches.map { _._2.minimumChoiceQueueSize }.min
      if (a < b) a else b

    case None => 0
  }

  override def resolve(resolver: BlockResolver, log: Logger) = {
    if (!branches.forall { case (_, b) => b.resolve(resolver, log) != None }) {
      None
    } else {
      elseB match {
        case None => Some(())
        case Some(b) => b.resolve(resolver, log)
      }
    }
  }

  override def toString = {
    val sb = new StringBuilder
    branches foreach {
      case (expr, block) if sb.length == 0 =>
        sb ++= s"If ${expr}\n"
        sb ++= block.toString
      case (expr, block) =>
        sb ++= s"Else If ${expr}\n"
        sb ++= block.toString
    }

    elseB match {
      case Some(b) =>
        sb ++= s"Else\n"
        sb ++= elseB.toString
      case None => {}
    }

    sb ++= "End\n\n"
    sb.toString
  }
}

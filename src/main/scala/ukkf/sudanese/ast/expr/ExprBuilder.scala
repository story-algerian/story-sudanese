/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.expr

import ukkf.sudanese.ast.ops._
import ukkf.sudanese.i18n.ListBuilder
import ukkf.sudanese.io.err.UnexpectedToken
import ukkf.sudanese.io.Logger
import ukkf.sudanese.tok._
import ukkf.sudanese.vars.Variables

object ExprBuilder {
  def buildExpr(vars: Variables, in: TokenReader, log: Logger): Option[Expr] = {
    val lhs = buildPrimaryExpr(vars, in, log) match {
      case None => return None
      case Some(e) => e
    }

    buildBinOpExpr(vars, in, 0, lhs, log)
  }

  private def buildBinOpExpr(vars: Variables, in: TokenReader,
                             prec: Int, _lhs: Expr, log: Logger): Option[Expr] = {
    var lhs = _lhs
    while (!in.eof) {
      val tok = in.getToken() match {
        case None => return None
        case Some(tok) => tok
      }

      val op = tokenToOp(tok) match {
        case Some(op) => op
        case None =>
          in.putBack(tok)
          return Some(lhs)
      }

      val tokPrec = getBinOpPrecedence(op)
      if (tokPrec < prec) {
        return Some(lhs)
      }

      var rhs = buildPrimaryExpr(vars, in, log) match {
        case None => return None
        case Some(e) => e
      }

      val nextPrec = in.getToken() match {
        case None => return None
        case Some(t) =>
          in.putBack(t)
          getBinOpPrecedence(t)
      }

      if (tokPrec < nextPrec) {
        rhs = buildBinOpExpr(vars, in, tokPrec + 1, rhs, log) match {
          case None => return None
          case Some(e) => e
        }
      }

      lhs = BinOpExpr(tok.line, tok.column, op, lhs, rhs)
    }

    Some(lhs)
  }

  private def buildParenExpr(vars: Variables, in: TokenReader,
                             log: Logger): Option[Expr] =
    buildExpr(vars, in, log).flatMap { ret =>
      in.getToken().flatMap { t =>
        t match {
          case _: RightParenthesisToken => Some(ret)
          case t =>
            log.err(t.line, Some(t.column), UnexpectedToken(t, "')'"))
            None
        }
      }
    }

  private def buildPrimaryExpr(vars: Variables, in: TokenReader,
                               log: Logger): Option[Expr] =
    in.getToken().flatMap { _ match {
      // variable
      case IdToken(l, c, id) => Some(VariableExpr(l, c, vars, id))

      // parenthesized expr
      case LeftParenthesisToken(_, _) => buildParenExpr(vars, in, log)

      // literals
      case IntLitToken(l, c, num) => Some(IntLitExpr(l, c, num))
      case RatLitToken(l, c, num) => Some(RatLitExpr(l, c, num))
      case StrLitToken(l, c, str) => Some(StringLitExpr(l, c, str))

      // unops
      case IntegerToken(l, c) => buildExpr(vars, in, log).map { e =>
        UnOpExpr(l, c, ToIntOp(), e)
      }
      case RationalToken(l, c) => buildExpr(vars, in, log).map { e =>
        UnOpExpr(l, c, ToRatOp(), e)
      }
      case StringToken(l, c) => buildExpr(vars, in, log).map { e =>
        UnOpExpr(l, c, ToStringOp(), e)
      }
      case MinusToken(l, c) => buildExpr(vars, in, log).map { e =>
        UnOpExpr(l, c, NegOp(), e)
      }
      case TildeToken(l, c) => buildExpr(vars, in, log).map { e =>
        UnOpExpr(l, c, NotOp(), e)
      }

      case t: Token =>
        log.err(t.line, Some(t.column),
          UnexpectedToken(t,
            ListBuilder.buildOrList(Token.msgs.getString("identifier"), "'('",
              Token.msgs.getString("literal"), Token.msgs.getString("unop"))))
        None
    }}

  private def tokenToOp(tok: Token): Option[BinOp] = Some(tok match {
    case PlusToken(_, _) => AddOp()
    case MinusToken(_, _) => SubOp()
    case AsteriskToken(_, _) => MulOp()
    case SlashToken(_, _) => DivOp()
    case PercentToken(_, _) => ModOp()

    case EqualsToken(_, _) => EqOp()
    case NotEqualToken(_, _) => NeqOp()
    case LessThanToken(_, _) => LtOp()
    case LessThanEqualsToken(_, _) => LteOp()
    case GreaterThanToken(_, _) => GtOp()
    case GreaterThanEqualsToken(_, _) => GteOp()

    case AmpersandToken(_, _) => AndOp()
    case VerticalLineToken(_, _) => OrOp()
    case CircumflexToken(_, _) => XorOp()

    case _ => return None
  })

  private def getBinOpPrecedence(tok: Token): Int = tokenToOp(tok) match {
    case Some(op) => getBinOpPrecedence(op)
    case None => -1
  }

  private def getBinOpPrecedence(op: BinOp): Int = op match {
    case MulOp() => 90
    case DivOp() => 90
    case ModOp() => 90

    case AddOp() => 80
    case SubOp() => 80

    case EqOp()  => 70
    case NeqOp() => 70
    case LtOp()  => 70
    case LteOp() => 70
    case GtOp()  => 70
    case GteOp() => 70

    case AndOp() => 60

    case XorOp() => 50

    case OrOp()  => 40
  }
}

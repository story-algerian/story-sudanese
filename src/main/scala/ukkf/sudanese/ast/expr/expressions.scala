/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.expr

import ukkf.sudanese.ast.ops._
import ukkf.sudanese.io.err.{UnsupportedOp, UninitializedVar, TypeMismatch}
import ukkf.sudanese.io.Logger
import ukkf.sudanese.types._
import ukkf.sudanese.vars.Variables

@SerialVersionUID(201L)
sealed abstract class Expr(val line: Int, val column: Int)
    extends Serializable {
  def getType(log: Logger): Option[Type]
  def getValue(log: Logger): Option[Any]
  def toString: String
}

final case class IntLitExpr(l: Int, c: Int, lit: BigInt)
    extends Expr(l, c) {
  override def getType(log: Logger) = Some(Type.Integer)
  override def getValue(log: Logger): Option[BigInt] = Some(lit)
  override def toString = lit.toString
}

final case class RatLitExpr(l: Int, c: Int, lit: BigDecimal)
    extends Expr(l, c) {
  override def getType(log: Logger) = Some(Type.Rational)
  override def getValue(log: Logger): Option[BigDecimal] = Some(lit)
  override def toString = lit.toString
}

final case class StringLitExpr(l: Int, c: Int, lit: String)
    extends Expr(l, c) {
  override def getType(log: Logger) = Some(Type.Rational)
  override def getValue(log: Logger): Option[String] = Some(lit)
  override def toString = "\"" + lit + "\""
}

// TODO: Add True, False keywords
final case class BoolLitExpr(l: Int, c: Int, lit: Boolean)
    extends Expr(l, c) {
  override def getType(log: Logger) = Some(Type.Bool)
  override def getValue(log: Logger): Option[Boolean] = Some(lit)
  override def toString = lit.toString
}

final case class VariableExpr(l: Int, c: Int, ctx: Variables, id: String)
    extends Expr(l, c) {
  override def getType(log: Logger): Option[Type] = ctx.getVar(id, l, c, log) match {
    case None => None
    case Some(v) => Some(v.tpe)
  }

  override def getValue(log: Logger): Option[Any] = ctx.getVar(id, l, c, log) match {
    case None => None
    case Some(v) => v.tpe match {
      case Type.None =>
        log.err(l, Some(c), UninitializedVar(id))
        None

      case _ => Some(v.value)
    }
  }

  override def toString = id
}

final case class UnOpExpr(l: Int, c: Int, op: UnOp, rhs: Expr)
    extends Expr(l, c) {
  override def toString = s"${op.toString} (${rhs.toString})"

  override def getType(log: Logger): Option[Type] = Some(op match {
    case ToIntOp() => Type.Integer
    case ToRatOp() => Type.Rational
    case ToStringOp() => Type.String
    case _ => return rhs.getType(log)
  })

  override def getValue(log: Logger): Option[Any] = {
    val t = rhs.getType(log) match {
      case None => return None
      case Some(t) => t
    }

    val rval = rhs.getValue(log) match {
      case None => return None
      case Some(v) => v
    }

    Some(op match {
      case ToIntOp() => t match {
        case Type.Integer => rval
        case Type.Rational => rval.asInstanceOf[BigDecimal].toBigInt
        case Type.String => BigInt(rval.toString)
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case ToRatOp() => t match {
        case Type.Integer => BigDecimal(rval.asInstanceOf[BigInt])
        case Type.Rational => rval
        case Type.String => BigDecimal(rval.toString)
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case ToStringOp() => rval.toString

      case NegOp() => t match {
        case Type.Integer => -rval.asInstanceOf[BigInt]
        case Type.Rational => -rval.asInstanceOf[BigDecimal]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case NotOp() => t match {
        case Type.Integer => ~rval.asInstanceOf[BigInt]
        case Type.Bool => !rval.asInstanceOf[Boolean]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }
    })
  }
}

final case class BinOpExpr(l: Int, c: Int, op: BinOp, lhs: Expr, rhs: Expr)
    extends Expr(l, c) {
  override def toString = s"(${lhs}) ${op} (${rhs})"

  override def getType(log: Logger): Option[Type] = Some(op match {
    case EqOp() => Type.Bool
    case NeqOp() => Type.Bool
    case GtOp() => Type.Bool
    case GteOp() => Type.Bool
    case LtOp() => Type.Bool
    case LteOp() => Type.Bool
    case _ => return lhs.getType(log)
  })

  override def getValue(log: Logger): Option[Any] = {
    val ltpe: Type = lhs.getType(log) match {
      case None => return None
      case Some(t) => t
    }

    val rtpe: Type = rhs.getType(log) match {
      case None => return None
      case Some(t) => t
    }

    val t: Type = if (ltpe != rtpe) {
      log.err(line, Some(column), TypeMismatch(ltpe, rtpe))
      return None
    } else {
      ltpe
    }

    val lval = lhs.getValue(log) match {
      case None => return None
      case Some(v) => v
    }

    val rval = rhs.getValue(log) match {
      case None => return None
      case Some(v) => v
    }

    Some(op match {
      case AddOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] + rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] + rval.asInstanceOf[BigDecimal]
        case Type.String => s"${lval.toString}${rval.toString}"
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case SubOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] - rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] - rval.asInstanceOf[BigDecimal]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case MulOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] * rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] * rval.asInstanceOf[BigDecimal]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case DivOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] / rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] / rval.asInstanceOf[BigDecimal]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case ModOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] % rval.asInstanceOf[BigInt]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case EqOp() => lval.equals(rval)
      case NeqOp() => !lval.equals(rval)

      case LtOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] < rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] < rval.asInstanceOf[BigDecimal]
        case Type.String => lval.toString < rval.toString
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case LteOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] <= rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] <= rval.asInstanceOf[BigDecimal]
        case Type.String => lval.toString <= rval.toString
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case GtOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] > rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] > rval.asInstanceOf[BigDecimal]
        case Type.String => lval.toString > rval.toString
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case GteOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] >= rval.asInstanceOf[BigInt]
        case Type.Rational =>
          lval.asInstanceOf[BigDecimal] >= rval.asInstanceOf[BigDecimal]
        case Type.String => lval.toString >= rval.toString
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case AndOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] & rval.asInstanceOf[BigInt]
        case Type.Bool =>
          lval.asInstanceOf[Boolean] && rval.asInstanceOf[Boolean]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }

      case OrOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] | rval.asInstanceOf[BigInt]
        case Type.Bool =>
          lval.asInstanceOf[Boolean] || rval.asInstanceOf[Boolean]
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
      }

      case XorOp() => t match {
        case Type.Integer =>
          lval.asInstanceOf[BigInt] ^ rval.asInstanceOf[BigInt]
        case Type.Bool =>
          val a = lval.asInstanceOf[Boolean]
          val b = rval.asInstanceOf[Boolean]
          (a || b) && !(a && b)
        case _ =>
          log.err(line, Some(column), UnsupportedOp(op.toString, t))
          return None
      }
    })
  }
}

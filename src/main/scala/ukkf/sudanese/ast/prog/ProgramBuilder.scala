/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.prog

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import ukkf.sudanese.ast.block._
import ukkf.sudanese.io.Logger
import ukkf.sudanese.io.err.UnexpectedToken
import ukkf.sudanese.tok._
import ukkf.sudanese.vars.Variables

final object ProgramBuilder {
  def buildProg(in: Source, log: Logger): Option[Program] =
    buildProg(TokenReader(in, log), log)

  def buildProg(in: TokenReader, log: Logger): Option[Program] = {
    val vars = new Variables
    val blocks = new ArrayBuffer[(Int, String, CodeBlock)]
    var continue = true
    val initBlock: CodeBlock = BlockBuilder.buildBlock(vars, in, log) match {
      case None => return None
      case Some(b) => b
    }

    while (continue) {
      in.getToken() match {
        case None => return None
        case Some(t) => t match {
          case NewlineToken(_, _) => // do nothing
          case EofToken(_, _) => continue = false
          case LabelToken(l, _) =>
            in.getToken() match {
              case None => return None
              case Some(t) => t match {
                case IdToken(_, _, id) =>
                  in.endLine()
                  val block: CodeBlock = BlockBuilder.buildBlock(vars, in, log) match {
                    case None => return None
                    case Some(b) => b
                  }

                  blocks += new Tuple3(l, id, block)

                case t =>
                  log.err(t.line, Some(t.column),
                    UnexpectedToken(t, Token.msgs.getString("identifier")))
                  return None
              }
            }

          case t =>
            log.err(t.line, Some(t.column), UnexpectedToken(t, "Label"))
            return None
        }
      }
    }

    val resolver = new BlockResolver(blocks)

    initBlock.resolve(resolver, log) match {
      case None => return None
      case Some(_) => {}
    }

    blocks.foreach { case (_, _, b) =>
      b.resolve(resolver, log) match {
        case None => return None
        case Some(_) => {}
      }
    }

    initBlock.checkChoiceQueue(log) match {
      case None => return None
      case Some(_) =>
        blocks.foreach {
          case (_, _, b) => b.checkChoiceQueue(log) match {
            case None => return None
            case Some(_) => {}
          }
        }
    }

    Some(Program(vars, initBlock))
  }
}

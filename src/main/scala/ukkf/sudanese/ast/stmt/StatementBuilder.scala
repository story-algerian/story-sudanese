/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.stmt

import java.util.regex.Pattern

import ukkf.sudanese.ast.block.BlockBuilder
import ukkf.sudanese.ast.expr.ExprBuilder
import ukkf.sudanese.i18n.ListBuilder
import ukkf.sudanese.io.err._
import ukkf.sudanese.io.warn.{ImplicitGoTo, UnknownColor}
import ukkf.sudanese.io.{Color, Logger}
import ukkf.sudanese.tok._
import ukkf.sudanese.vars.Variables

object StatementBuilder {
  def buildStmt(vars: Variables, in: TokenReader, log: Logger): Option[Statement] =
    getLegitToken(in).flatMap { _ match {
      // Add Choice
      case MinusToken(l, c) => in.getLine().flatMap { line =>
        in.endLine().flatMap { _ =>
          val split = line.split("->", 2)
          val text = split.head.trim
          val target = split.tail.head.trim

          if (target.length == 0) {
            log.err(l, Some(c), EmptyChoiceTarget)
            None
          } else if (text.length == 0) {
            log.err(l, Some(c), EmptyChoiceText)
            None
          } else {
            Some(AddChoiceStatement(l, text, target))
          }
        }
      }

      // Add Anonymous Choice
      case RightArrowToken(l, c) => in.getLine().map { _.trim }.flatMap { line =>
        in.endLine().flatMap { _ =>
          if (line.length == 0) {
            log.err(l, Some(c), EmptyChoiceTarget)
            None
          } else {
            Some(AddChoiceStatement(l, line, line))
          }
        }
      }

      // Assign
      case IdToken(l, _, id) => in.getToken().flatMap { _ match {
        case AssignToken(_, _) => ExprBuilder.buildExpr(vars, in, log).flatMap { e =>
          in.endLine().map { _ => AssignStatement(l, id, e) }
        }

        case t =>
          log.err(t.line, Some(t.column), UnexpectedToken(t, "':='"))
          None
      }}

      // BadEnd
      case BadEndToken(l, _) => in.endLine().map { _ => BadEndStatement(l) }

      // Choose
      case ChooseToken(l, _) => in.endLine().map { _ => ChooseStatement(l) }

      // ChooseRandom
      case ChooseRandomToken(l, _) => in.endLine().map { _ =>
        ChooseRandomStatement(l)
      }

      // Clear
      case ClearToken(l, _) => in.endLine().map { _ => ClearStatement(l) }

      // Color
      // TODO: Add all colours as keywords
      case ColorToken(l, _) => in.getToken().flatMap { _ match {
        case IdToken(l, c, id) => Color.withNameOption(id) match {
          case Some(col) =>
            in.endLine().map { _ => ColorStatement(l, col) }

          case None =>
            log.warn(l, Some(c), UnknownColor(id))
            Some(NoOpStatement)
        }

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("color_id")))
          None
      }}

      // Else, Else If
      case ElseToken(l, _) => in.getToken().flatMap { _ match {
        // Else If
        case IfToken(_, _) => ExprBuilder.buildExpr(vars, in, log).flatMap { expr =>
          in.endLine().map { _ => ElseIfStatement(l, expr) }
        }

        // Else
        case NewlineToken(_, _) => Some(ElseStatement(l))

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t,
              ListBuilder.buildOrList("'If'", Token.msgs.getString("new_line"))))
          None
      }}

      // End
      case EndToken(l, _) => in.endLine().map { _ => EndStatement(l) }

      // GoodEnd
      case GoodEndToken(l, _) => in.endLine().map { _ => GoodEndStatement(l) }

      // GoTo
      case GoToToken(l, _) => in.getToken().flatMap { _ match {
        case IdToken(_, _, id) =>
          in.endLine().map { _ => GoToStatement(l, id) }

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("identifier")))
          None
      }}

      // If
      case IfToken(l, _) => ExprBuilder.buildExpr(vars, in, log).flatMap { expr =>
        BlockBuilder.buildIfBlock(vars, in, expr, l, log)
      }.map { block => IfStatement(l, block) }

      // Label
      case lt @ LabelToken(l, c) => in.getToken().flatMap { _ match {
        case it @ IdToken(_, _, id) =>
          in.putBack(it)
          in.putBack(lt)
          log.warn(l, Some(c), ImplicitGoTo(id))
          Some(ImplicitGoToStatement(l, id))

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("identifier")))
          None
      }}

      // Out
      case GreaterThanToken(l, _) => in.getLine().flatMap { text =>
        in.endLine().map { _ =>
          val indent = Pattern.compile("^>+").matcher(text)
          val text2 = if (indent.find) {
            " " * indent.end + text.substring(indent.end).trim
          } else {
            text.trim
          }

          OutStatement(l, text2)
        }
      }

      // Sleep
      case SleepToken(l, _) => in.getToken().flatMap { _ match {
        case IntLitToken(_, _, num) =>
          in.endLine().map { _ => SleepStatement(l, num) }

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("integer_lit")))
          None
      }}

      // SlowType
      case SlowTypeToken(l, _) => in.getToken().flatMap { _ match {
        case IntLitToken(_, _, num) =>
          in.endLine().map { _ => SlowTypeStatement(l, num) }

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("integer_lit")))
          None
      }}

      // Var
      // TODO: I think this should be improved upon
      case VarToken(l, _) => in.getToken().flatMap { _ match {
        case IdToken(_, _, id) =>
          in.endLine().flatMap { _ =>
            vars.defineVar(id, l, log)
          }.flatMap { _ =>
            buildStmt(vars, in, log)
          }

        case t =>
          log.err(t.line, Some(t.column),
            UnexpectedToken(t, Token.msgs.getString("identifier")))
          None
      }}

      // Wait
      case WaitToken(l, _) => in.endLine().map { _ => WaitStatement(l) }

      case t =>
        log.err(t.line, Some(t.column), InvalidStatement)
        None
    }}

  private def getLegitToken(in: TokenReader): Option[Token] = in.getToken() match {
    case None => None
    case Some(t) => t match {
      case NewlineToken(_, _) => getLegitToken(in)
      case t => Some(t)
    }
  }
}

/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.stmt

import ukkf.sudanese.ast.block.{CodeBlock, IfElseBlock}
import ukkf.sudanese.ast.expr.Expr
import ukkf.sudanese.exe.Executor
import ukkf.sudanese.io.{Logger, Color}

/** Provides a common interface to statements in Story African */
@SerialVersionUID(201L)
sealed abstract class Statement(val line: Int, _terminating: Boolean)
    extends Serializable {
  def branchEnd: Boolean = false
  def execute(exe: Executor, log: Logger): Option[Unit]
  def terminating: Boolean = _terminating
  def toString: String
}

final case class AddChoiceStatement(l: Int, str: String, target: String)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = None // TODO
  override def toString = s"- ${str} -> ${target} # ${l}\n"
}

final case class AddChoiceStatementFinal(l: Int, str: String, target: CodeBlock)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.addChoice(str, target, log)
  override def toString = "Add choice final\n"
}

final case class AssignStatement(l: Int, name: String, expr: Expr)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.vars.setVar(name, expr, l, log)
  override def toString = s"${name} := ${expr} # ${l}\n"
}

final case class BadEndStatement(l: Int) extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = exe.badEnd(log)
  override def toString = s"BadEnd # ${l}\n\n"
}

final case class ChooseStatement(l: Int) extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = exe.choose(log)
  override def toString = s"Choose # ${l}\n\n"
}

final case class ChooseRandomStatement(l: Int)
    extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = exe.chooseRandom(log)
  override def toString = s"ChooseRandom # ${l}\n\n"
}

final case class ClearStatement(l: Int) extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.clearScreen(log)
  override def toString = s"Clear # ${l}\n"
}

final case class ColorStatement(l: Int, col: Color)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.useColor(col, log)
  override def toString = s"Color # ${l}\n"
}

final case class ElseStatement(l: Int) extends Statement(l, false) {
  override def branchEnd = true
  override def execute(exe: Executor, log: Logger) = None
  override def toString = "<error> (Else)"
}

final case class ElseIfStatement(l: Int, expr: Expr)
    extends Statement(l, false) {
  override def branchEnd = true
  override def execute(exe: Executor, log: Logger) = None
  override def toString = "<error> (Else If)"
}

final case class EndStatement(l: Int) extends Statement(l, false) {
  override def branchEnd = true
  override def execute(exe: Executor, log: Logger) = None
  override def toString = "<error> (End)"
}

final case class GoodEndStatement(l: Int) extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = exe.goodEnd(log)
  override def toString = s"GoodEnd # ${l}\n\n"
}

final case class GoToStatement(l: Int, target: String)
    extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = None // TODO
  override def toString = s"GoTo ${target} # ${l}\n\n"
}

final case class GoToStatementFinal(l: Int, target: CodeBlock)
    extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = target.execute(exe, log)
  override def toString = "GoTo final"
}

final case class IfStatement(l: Int, block: IfElseBlock)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = block.execute(exe, log)
  override def terminating = block.isTerminated
  override def toString = block.toString
}

final case class ImplicitGoToStatement(l: Int, target: String)
    extends Statement(l, true) {
  override def execute(exe: Executor, log: Logger) = None // TODO
  override def toString = s"# implicit\nGoTo ${target} # ${l}\n\n"
}

final case object NoOpStatement extends Statement(0, false) {
  override def execute(exe: Executor, log: Logger) = Some(())
  override def toString = ""
}

final case class OutStatement(l: Int, str: String)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.output(str, log)
  override def toString = s"> ${str} # ${l}\n" // TODO: indentation
}

final case class SleepStatement(l: Int, time: BigInt)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.sleep(time, log)
  override def toString = s"Sleep ${time} # ${l}\n"
}

final case class SlowTypeStatement(l: Int, speed: BigInt)
    extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = Some(exe.speed = speed)
  override def toString = s"SlowType ${speed} # ${l}\n"
}

final case class WaitStatement(l: Int) extends Statement(l, false) {
  override def execute(exe: Executor, log: Logger) = exe.waitLine(log)
  override def toString = s"Wait # ${l}\n"
}

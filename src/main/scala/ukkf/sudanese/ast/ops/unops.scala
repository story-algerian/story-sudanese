/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.ast.ops

import ukkf.sudanese.i18n.Messages

private object UnOp {
  val msgs = Messages.getMsgs("ops")
}

@SerialVersionUID(100L)
sealed abstract class UnOp(val key: String) extends Serializable {
  override def toString: String = UnOp.msgs.getString(key)
}

final case class ToIntOp() extends UnOp("to_int")
final case class ToRatOp() extends UnOp("to_rat")
final case class ToStringOp() extends UnOp("to_string")

final case class NegOp() extends UnOp("neg")
final case class NotOp() extends UnOp("not")


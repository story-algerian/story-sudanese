/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.tok

import scala.io.Source

import ukkf.sudanese.io.{CountedReader, Logger}
import ukkf.sudanese.io.err.{ExpectedDigit, ExpectedNewline, UnexpectedToken, UnknownCharacter}

class TokenReader(in: CountedReader, log: Logger) {
  private var nextChar = in.read()
  private var nextToken: List[Token] = Nil

  def endLine(): Option[Unit] = nextToken match {
    case tok :: rest =>
      tok match {
        case NewlineToken(_, _) =>
          nextToken = rest
          Some(())

        case t =>
          log.err(in.line, Some(in.column),
                  UnexpectedToken(t, Token.msgs.getString("new_line")))
          None
      }

    case Nil =>
      var nextCharLifted = nextChar match {
        case None => return None
        case Some(c) => c
      }

      while (Character.isWhitespace(nextCharLifted) && nextCharLifted != '\n') {
        nextChar = in.read()
        nextCharLifted = nextChar match {
          case None => return None
          case Some(c) => c
        }
      }

      if (nextCharLifted != '\n' && nextCharLifted != -1) {
        log.err(in.line, Some(in.column), ExpectedNewline(nextCharLifted))
        None
      } else {
        nextChar = in.read()
        Some(())
      }
  }

  def eof: Boolean = nextToken == Nil && in.eof

  /** Gets the next line as a string */
  def getLine(): Option[String] = nextChar match {
    case None => None
    case Some(c) =>
      in.putBack(c)
      val ret = in.readLine()
      nextChar = in.read()
      nextToken = NewlineToken(in.line, -1) :: nextToken
      ret
  }

  /** Gets the next available token */
  def getToken(): Option[Token] = {
    var nextCharLifted = nextChar match {
      case None => return None
      case Some(c) => c
    }

    if (!nextToken.isEmpty) {
      val ret = nextToken.head
      nextToken = nextToken.tail
      return Some(ret)
    }

    if (in.eof) {
      return Some(EofToken(in.line, in.column))
    }

    while (Character.isWhitespace(nextCharLifted) && nextCharLifted != '\n') {
      nextChar = in.read()
      nextCharLifted = nextChar match {
        case None => return None
        case Some(c) => c
      }
    }

    val line = in.line
    val column = in.column

    // Number
    if (nextCharLifted >= '0' && nextCharLifted <= '9') {
      val sb = new StringBuilder
      do {
        sb ++= Character.toChars(nextCharLifted)
        nextChar = in.read()
        nextCharLifted = nextChar match {
          case None => return None
          case Some(c) => c
        }
      } while (nextCharLifted >= '0' && nextCharLifted <= '9')

      val integer = sb.toString

      // Integer
      if (nextCharLifted != '.' && nextCharLifted != 'e' && nextCharLifted != 'E') {
        return Some(IntLitToken(line, column, BigInt(integer)))
      }

      // Rational
      val fraction = if (nextCharLifted == '.') {
        sb.clear()
        nextChar = in.read()
        nextCharLifted = nextChar match {
          case None => return None
          case Some(c) => c
        }

        if (nextCharLifted < '0' || nextCharLifted > '9') {
          log.err(in.line, Some(in.column), ExpectedDigit(nextCharLifted))
          return None
        }

        do {
          sb ++= Character.toChars(nextCharLifted)
          nextChar = in.read()
          nextCharLifted = nextChar match {
            case None => return None
            case Some(c) => c
          }
        } while (nextCharLifted >= '0' && nextCharLifted <= '9')

        sb.toString
      } else {
        "0"
      }

      val exponent = if (nextCharLifted == 'e' || nextCharLifted == 'E') {
        sb.clear()
        nextChar = in.read()
        nextCharLifted = nextChar match {
          case None => return None
          case Some(c) => c
        }
        if (nextCharLifted == '+' || nextCharLifted == '-') {
          sb ++= Character.toChars(nextCharLifted)
          nextChar = in.read()
          nextCharLifted = nextChar match {
            case None => return None
            case Some(c) => c
          }
        }

        if (nextCharLifted < '0' || nextCharLifted > '9') {
          log.err(in.line, Some(in.column), ExpectedDigit(nextCharLifted))
          return None
        }

        do {
          sb ++= Character.toChars(nextCharLifted)
          nextChar = in.read()
          nextCharLifted = nextChar match {
            case None => return None
            case Some(c) => c
          }
        } while (nextCharLifted >= '0' && nextCharLifted <= '9')

        sb.toString
      } else {
        "0"
      }

      val num = BigDecimal(s"${integer}.${fraction}e${exponent}")
      return Some(RatLitToken(line, column, num))
    }

    // Identifier or a Keyword
    if (Character.isLetter(nextCharLifted)) {
      val sb = new StringBuilder
      do {
        sb ++= Character.toChars(nextCharLifted)
        nextChar = in.read()
        nextCharLifted = nextChar match {
          case None => return None
          case Some(c) => c
        }
      } while (nextCharLifted == '_' || Character.isLetterOrDigit(nextCharLifted))

      val id = sb.toString
      val tok = id match {
        case "BadEnd" => BadEndToken(line, column)
        case "Choose" => ChooseToken(line, column)
        case "ChooseRandom" => ChooseRandomToken(line, column)
        case "Clear" => ClearToken(line, column)
        case "Color" => ColorToken(line, column)
        case "Else" => ElseToken(line, column)
        case "End" => EndToken(line, column)
        case "Rational" => RationalToken(line, column)
        case "GoodEnd" => GoodEndToken(line, column)
        case "GoTo" => GoToToken(line, column)
        case "If" => IfToken(line, column)
        case "Integer" => IntegerToken(line, column)
        case "Label" => LabelToken(line, column)
        case "Sleep" => SleepToken(line, column)
        case "SlowType" => SlowTypeToken(line, column)
        case "String" => StringToken(line, column)
        case "Var" => VarToken(line, column)
        case "Wait" => WaitToken(line, column)

        case _ => IdToken(line, column, id)
      }

      return Some(tok)
    }

    val ret = nextChar match {
      case None => return None
      case Some(c) => c match {
        case '&' => AmpersandToken(line, column)
        case ':' =>
          nextChar = in.read()
          nextChar match {
            case None => return None
            case Some(c) =>
              if (c == '=') {
                AssignToken(line, column)
              } else {
                in.putBack(c)
                in.putBack(':')
                log.err(line, Some(column), UnknownCharacter(':'))
                return None
              }
          }
        case '*' => AsteriskToken(line, column)
        case '^' => CircumflexToken(line, column)
        case '=' => EqualsToken(line, column)
        case '>' =>
          nextChar = in.read()
          nextChar match {
            case None => return None
            case Some(c) =>
              if (c == '=') {
                GreaterThanEqualsToken(line, column)
              } else {
                in.putBack(c)
                GreaterThanToken(line, column)
              }
          }
        case '(' => LeftParenthesisToken(line, column)
        case '<' =>
          nextChar = in.read()
          nextChar match {
            case None => return None
            case Some(c) =>
              if (c == '=') {
                LessThanEqualsToken(line, column)
              } else {
                in.putBack(c)
                LessThanToken(line, column)
              }
          }
        case '-' =>
          nextChar = in.read()
          nextChar match {
            case None => return None
            case Some(c) =>
              if (c == '>') {
                RightArrowToken(line, column)
              } else {
                in.putBack(c)
                MinusToken(line, column)
              }
          }
        case '%' => PercentToken(line, column)
        case '+' => PlusToken(line, column)
        case '/' => SlashToken(line, column)
        case ')' => RightParenthesisToken(line, column)
        case '"' =>
          val ret = StrLitToken(line, column,
            in.readUntil('"') match {
              case None => return None
              case Some(str) => str
          })
          in.read() // get rid of the following "
          ret
        case '~' =>
          nextChar = in.read()
          nextChar match {
            case None => return None
            case Some(c) =>
              if (c == '=') {
                NotEqualToken(line, column)
              } else {
                in.putBack(c)
                TildeToken(line, column)
              }
          }
        case '|' => VerticalLineToken(line, column)

        case '\n' => NewlineToken(line, column)
        case -1 => return Some(EofToken(line, column))

        case c =>
          log.err(line, Some(column), UnknownCharacter(c))
          return None
      }
    }

    nextChar = in.read()

    Some(ret)
  }

  /** "Puts back" a token previously read into the token stream */
  def putBack(tw: Token): Unit =
    nextToken = tw :: nextToken
}

object TokenReader {
  def apply(in: Source, log: Logger): TokenReader =
    new TokenReader(new CountedReader(in, log), log)
}

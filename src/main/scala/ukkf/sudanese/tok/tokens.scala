/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.tok

import ukkf.sudanese.i18n.Messages

sealed abstract class Token(val line: Int, val column: Int)

case class IdToken(l: Int, c: Int, val id: String) extends Token(l, c)

// Literals

case class IntLitToken(l: Int, c: Int, val num: BigInt) extends Token(l, c)
case class RatLitToken(l: Int, c: Int, val num: BigDecimal) extends Token(l, c)
case class StrLitToken(l: Int, c: Int, val str: String) extends Token(l, c)

// Keywords

case class BadEndToken(l: Int, c: Int) extends Token(l, c)
case class ChooseToken(l: Int, c: Int) extends Token(l, c)
case class ChooseRandomToken(l: Int, c: Int) extends Token(l, c)
case class ClearToken(l: Int, c: Int) extends Token(l, c)
case class ColorToken(l: Int, c: Int) extends Token(l, c)
case class ElseToken(l: Int, c: Int) extends Token(l, c)
case class EndToken(l: Int, c: Int) extends Token(l, c)
case class RationalToken(l: Int, c: Int) extends Token(l, c)
case class GoodEndToken(l: Int, c: Int) extends Token(l, c)
case class GoToToken(l: Int, c: Int) extends Token(l, c)
case class IfToken(l: Int, c: Int) extends Token(l, c)
case class IntegerToken(l: Int, c: Int) extends Token(l, c)
case class LabelToken(l: Int, c: Int) extends Token(l, c)
case class SleepToken(l: Int, c: Int) extends Token(l, c)
case class SlowTypeToken(l: Int, c: Int) extends Token(l, c)
case class StringToken(l: Int, c: Int) extends Token(l, c)
case class VarToken(l: Int, c: Int) extends Token(l, c)
case class WaitToken(l: Int, c: Int) extends Token(l, c)

// Operator precedences, higher ones bind first
// Integer, Rational, String, ~
// *, /, %
// +, -
// =, ~=, >, >=, <, <=
// &
// ^
// |

// Tokens from special symbols

case class AmpersandToken(l: Int, c: Int) extends Token(l, c)
case class AssignToken(l: Int, c: Int) extends Token(l, c)
case class AsteriskToken(l: Int, c: Int) extends Token(l, c)
case class CircumflexToken(l: Int, c: Int) extends Token(l, c)
case class EqualsToken(l: Int, c: Int) extends Token(l, c)
case class GreaterThanEqualsToken(l: Int, c: Int) extends Token(l, c)
case class GreaterThanToken(l: Int, c: Int) extends Token(l, c)
case class LeftParenthesisToken(l: Int, c: Int) extends Token(l, c)
case class LessThanEqualsToken(l: Int, c: Int) extends Token(l, c)
case class LessThanToken(l: Int, c: Int) extends Token(l, c)
case class MinusToken(l: Int, c: Int) extends Token(l, c)
case class NotEqualToken(l: Int, c: Int) extends Token(l, c)
case class PercentToken(l: Int, c: Int) extends Token(l, c)
case class PlusToken(l: Int, c: Int) extends Token(l, c)
case class RightArrowToken(l: Int, c: Int) extends Token(l, c)
case class RightParenthesisToken(l: Int, c: Int) extends Token(l, c)
case class SlashToken(l: Int, c: Int) extends Token(l, c)
case class TildeToken(l: Int, c: Int) extends Token(l, c)
case class VerticalLineToken(l: Int, c: Int) extends Token(l, c)

// Other tokens

case class EofToken(l: Int, c: Int) extends Token(l, c)
case class NewlineToken(l: Int, c: Int) extends Token(l, c)

object Token {
  val msgs = Messages.getMsgs("token")

  def toString(t: Token): String = t match {
    case IdToken(_, _, _) => s"{${msgs.getString("identifier")}}"

    case IntLitToken(_, _, _) => s"{${msgs.getString("integer_lit")}}"
    case RatLitToken(_, _, _) => s"{${msgs.getString("rat_lit")}}"
    case StrLitToken(_, _, _) => s"{${msgs.getString("string_lit")}}"

    case BadEndToken(_, _) => "BadEnd"
    case ChooseToken(_, _) => "Choose"
    case ChooseRandomToken(_, _) => "ChooseRandom"
    case ClearToken(_, _) => "Clear"
    case ColorToken(_, _) => "Color"
    case ElseToken(_, _) => "Else"
    case EndToken(_, _) => "End"
    case RationalToken(_, _) => "Rational"
    case GoodEndToken(_, _) => "GoodEnd"
    case GoToToken(_, _) => "GoTo"
    case IfToken(_, _) => "If"
    case IntegerToken(_, _) => "Integer"
    case LabelToken(_, _) => "Label"
    case SleepToken(_, _) => "Sleep"
    case SlowTypeToken(_, _) => "SlowType"
    case StringToken(_, _) => "String"
    case VarToken(_, _) => "Var"
    case WaitToken(_, _) => "Wait"

    case AmpersandToken(_, _) => "'&'"
    case AssignToken(_, _) => "':='"
    case AsteriskToken(_, _) => "'*'"
    case CircumflexToken(_, _) => "'^'"
    case EqualsToken(_, _) => "'='"
    case GreaterThanEqualsToken(_, _) => "'>='"
    case GreaterThanToken(_, _) => "'>'"
    case LeftParenthesisToken(_, _) => "'('"
    case LessThanEqualsToken(_, _) => "'<='"
    case LessThanToken(_, _) => "'<'"
    case MinusToken(_, _) => "'-'"
    case NotEqualToken(_, _) => "'~='"
    case PercentToken(_, _) => "'%'"
    case PlusToken(_, _) => "'+'"
    case RightArrowToken(_, _) => "'->'"
    case RightParenthesisToken(_, _) => "')'"
    case SlashToken(_, _) => "'/'"
    case TildeToken(_, _) => "'~'"
    case VerticalLineToken(_, _) => "'|'"

    case EofToken(_, _) => s"<${msgs.getString("eof")}>"
    case NewlineToken(_, _) => s"<${msgs.getString("new_line")}>"
  }
}

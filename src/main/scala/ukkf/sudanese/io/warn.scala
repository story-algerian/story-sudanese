/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io.warn

import ukkf.sudanese.i18n.Messages

sealed abstract class WarnMsg(key: String) {
  final def msg: String = WarnMsg.msgs.getString(key)
}

object WarnMsg {
  private val msgs = Messages.getMsgs("warnings")

  def toString(w: WarnMsg): String = w match {
    case EmptyElse => w.msg
    case EmptyElseIf => w.msg
    case EmptyIf => w.msg
    case ImplicitGoTo(id) => Messages.fmt(w.msg, id)
    case UnknownColor(id) => Messages.fmt(w.msg, id)
    case UnreachableCode => w.msg
  }
}

final case object EmptyElse extends WarnMsg("empty_else")
final case object EmptyElseIf extends WarnMsg("empty_elseif")
final case object EmptyIf extends WarnMsg("empty_if")
final case class  ImplicitGoTo(id: String) extends WarnMsg("implicit_goto")
final case class  UnknownColor(id: String) extends WarnMsg("unknown_color")
final case object UnreachableCode extends WarnMsg("unreachable_code")

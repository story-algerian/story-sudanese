/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io

import enumeratum._

sealed trait Color extends EnumEntry

object Color extends Enum[Color] {
  val values = findValues

  case object Black   extends Color
  case object Blue    extends Color
  case object Green   extends Color
  case object Cyan    extends Color
  case object Red     extends Color
  case object Magenta extends Color
  case object Brown   extends Color
  case object Gray    extends Color

  case object DarkGray      extends Color
  case object BrightBlue    extends Color
  case object BrightGreen   extends Color
  case object BrightCyan    extends Color
  case object BrightRed     extends Color
  case object BrightMagenta extends Color
  case object Yellow        extends Color
  case object White         extends Color
}

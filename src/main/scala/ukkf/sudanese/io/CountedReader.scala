/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io

import scala.io.Source
import ukkf.sudanese.io.err.{UnexpectedEofInUtf, UnexpectedEofWhileSearching}

/** Remembers the location in the source file.  Has a few other helper funcs.
  */
class CountedReader(in: Source, log: Logger) {
  private var _column = 0
  private var _line = 1
  private var _eof = false
  private var stack: List[Int] = Nil

  /** The current "column" in the input source */
  def column = _column
  /** The current "line" in the input source */
  def line = _line
  /** If the input line has reached its end */
  def eof = _eof

  /** "Puts back" a codepoint previously read into the input source.
    *
    * Note that if the character passed is a newline then column location is
    * lost.
    */
  def putBack(ch: Int): Unit = {
    stack = ch :: stack
    if (ch == '\n') {
      _line -= 1
      _column = -1
    } else {
      _column -= 1
    }
  }

  /** Reads a codepoint from the input source */
  def read(): Option[Int] = {
    if (!stack.isEmpty) {
      val ret = stack.head
      stack = stack.tail
      if (ret == '\n') {
        _line += 1
        _column = 0
      } else {
        _column += 1
      }

      return Some(ret)
    }

    if (_eof) {
      return Some(-1)
    }

    if (in.isEmpty) {
      _eof = true
      return Some(-1)
    }

    val first = in.next()

    val firstCh = first.toChar
    if (Character.isHighSurrogate(firstCh)) {
      if (in.isEmpty) {
        _eof = true
        log.err(_line, Some(_column), UnexpectedEofInUtf)
        return None
      }

      val second = in.next()

      val secondCh = second.toChar
      if (Character.isLowSurrogate(secondCh)) {
        _column += 1
        Some(Character.codePointAt(Array(firstCh, secondCh), 0))
      } else {
        log.err(_line, Some(_column), UnexpectedEofInUtf)
        None
      }
    } else {
      if (first == '\n') {
        _line += 1
        _column = 0
      } else {
        _column += 1
      }

      if (first == '#') {
        skipComment()
        Some('\n')
      } else {
        Some(first)
      }
    }
  }

  /** Reads a line from the input source */
  def readLine(): Option[String] = readUntil('\n') match {
    case None => None
    case Some(str) =>
      read() // To get rid of the \n
      Some(s"${str}\n")
  }

  /** Reads from input source until reaching the specified codepoint.
    *
    * The end codepoint isn't put in the resulting string, and is returned back
    * to the input source.
    */
  def readUntil(end: Int): Option[String] = {
    val sb = new StringBuilder
    var next = read() match {
      case None => return None
      case Some(c) => c
    }

    while (next != end) {
      sb ++= Character.toChars(next)
      next = read() match {
        case None => return None
        case Some(c) => c
      }

      if (eof) {
        log.err(_line, Some(_column), UnexpectedEofWhileSearching(end))
        return None
      }
    }

    putBack(next)
    Some(sb.toString)
  }

  private def skipComment(): Option[Unit] = {
    var next = read()
    while (next != None && next != Some('\n')) {
      next = read()
    }

    next match {
      case None => None
      case Some(_) => Some(())
    }
  }
}

/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io.err

import ukkf.sudanese.i18n.Messages
import ukkf.sudanese.tok.Token
import ukkf.sudanese.types.Type

sealed abstract class ErrMsg(key: String) {
  private final def msg: String = ErrMsg.msgs.getString(key)
}

object ErrMsg {
  private val msgs = Messages.getMsgs("errors")

  def toString(e: ErrMsg): String = e match {
    case ChoiceQueueEmpty => e.msg
    case ElseAfterElse(line) => Messages.fmt(e.msg, line)
    case EmptyChoiceTarget => e.msg
    case EmptyChoiceText => e.msg
    case ExpectedDigit(cp) => Messages.fmt(e.msg, Character.toChars(cp).mkString(""))
    case ExpectedNewline(cp) => Messages.fmt(e.msg, Character.toChars(cp).mkString(""))
    case InvalidStatement => e.msg
    case LabelInsideConditional => e.msg
    case LabelNotFound(id) => Messages.fmt(e.msg, id)
    case MultipleVarDef(varName, prevLine) => Messages.fmt(e.msg, varName, prevLine)
    case NotInIf => e.msg
    case RuntimeError(err) => Messages.fmt(e.msg, ErrMsg.toString(err))
    case TypeMismatch(t1, t2) => Messages.fmt(e.msg, t1.toString, t2.toString)
    case UnexpectedEofInUtf => e.msg
    case UnexpectedEofWhileSearching(cp) => Messages.fmt(e.msg, Character.toChars(cp).mkString(""))
    case UnexpectedToken(t, exp) => Messages.fmt(e.msg, Token.toString(t), exp)
    case UninitializedVar(name) => Messages.fmt(e.msg, name)
    case UnknownCharacter(cp) => Messages.fmt(e.msg, Character.toChars(cp).mkString(""))
    case UnknownIdentifier(name) => Messages.fmt(e.msg, name)
    case UnsupportedOp(op: String, t: Type) => Messages.fmt(e.msg, op, t.toString)
  }
}

final case object ChoiceQueueEmpty extends ErrMsg("choice_queue_empty")
final case class  ElseAfterElse(line: Int) extends ErrMsg("else_after_else")
final case object EmptyChoiceTarget extends ErrMsg("empty_choice_target")
final case object EmptyChoiceText extends ErrMsg("empty_choice_text")
final case class  ExpectedDigit(cp: Int) extends ErrMsg("expected_digit")
final case class  ExpectedNewline(cp: Int) extends ErrMsg("expected_newline")
final case object InvalidStatement extends ErrMsg("invalid_statement")
final case object LabelInsideConditional extends ErrMsg("label_inside_conditional")
final case class  LabelNotFound(id: String) extends ErrMsg("label_not_found")
final case class  MultipleVarDef(varName: String, prevLine: Int)
    extends ErrMsg("multiple_var_def")

final case object NotInIf extends ErrMsg("not_in_if")
final case class  RuntimeError(err: ErrMsg) extends ErrMsg("runtime_error")
final case class  TypeMismatch(t1: Type, t2: Type) extends ErrMsg("type_mismatch")
final case object UnexpectedEofInUtf extends ErrMsg("unexpected_eof_in_utf")
final case class  UnexpectedEofWhileSearching(cp: Int) extends ErrMsg("unexpected_eof")
final case class  UnexpectedToken(t: Token, exp: String)
    extends ErrMsg("unexpected_token")

final case class UninitializedVar(name: String) extends ErrMsg("uninitialized_var")
final case class UnknownCharacter(cp: Int) extends ErrMsg("unknown_character")
final case class UnknownIdentifier(name: String) extends ErrMsg("unknown_identifier")
final case class UnsupportedOp(op: String, t: Type) extends ErrMsg("unsupported_op")

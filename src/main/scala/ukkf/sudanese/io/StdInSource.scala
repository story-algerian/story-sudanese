/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io

import scala.io.{Source, StdIn}

/** Provides a [[scala.io.Source]] wrapper around
  * [[scala.io.StdIn$ scala.io.StdIn]]
  */
object StdInSource extends Source {
  object StdInIter extends Iterator[Char] {
    private var _next = getNext()
    private var idx = 0

    override def hasNext: Boolean = _next != None

    override def next(): Char = _next match {
      case None => throw new RuntimeException("No more characters available!")
      case Some(s) =>
        if (idx < s.length) {
          idx += 1
          s(idx - 1)
        } else if (idx == s.length) {
          idx = 0
          _next = getNext()
          '\n'
        } else {
          throw new RuntimeException("The code's fucked mate")
        }
    }

    private def getNext() = StdIn.readLine() match {
      case null => None
      case s => Some(s)
    }
  }

  val iter = StdInIter
}

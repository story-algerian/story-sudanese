/* Copyright (C) 2019  Uko Koknevics <perkontevs AT gmail DOT com>
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukkf.sudanese.io

import ukkf.sudanese.io.err.ErrMsg
import ukkf.sudanese.io.warn.WarnMsg

/** Manages output, such as errors, warnings, etc */
class Logger(file: String) {
  private val sb = new StringBuilder

  def err(line: Int, column: Option[Int], msg: ErrMsg): Unit = {
    column match {
      case Some(col) =>
        sb ++= s"error: ${file}:${line}:${col} -- ${ErrMsg.toString(msg)}\n"
      case None =>
        sb ++= s"error: ${file}:${line} -- ${ErrMsg.toString(msg)}\n"
    }
  }

  def warn(line: Int, column: Option[Int], msg: WarnMsg): Unit = column match {
    case Some(col) =>
      sb ++= s"warning: ${file}:${line}:${col} -- ${WarnMsg.toString(msg)}\n"
    case None =>
      sb ++= s"warning: ${file}:${line} -- ${WarnMsg.toString(msg)}\n"
  }

  override def toString(): String = sb.toString
}

object Logger {
  def apply(file: String): Logger = new Logger(file)
}
